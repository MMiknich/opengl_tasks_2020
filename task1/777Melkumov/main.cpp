#include <iostream>
#include <vector>
#include <queue>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <string.h>
// #include <time.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>


#define s2 0.7321f
#define def_camSpeed 0.5f
#define def_camSens 0.0005f
#define def_zoomSens 0.3f
#define mouse_click 1
#define y_mouse_inv -1 // -1 or 1
#define x_mouse_inv 1 // -1 or 1
#define BRUNKE 0
#define BRICK 1

GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path){
	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if(VertexShaderStream.is_open()){
		std::stringstream sstr;
		sstr << VertexShaderStream.rdbuf();
		VertexShaderCode = sstr.str();
		VertexShaderStream.close();
	}else{
	
		printf("Не возможно найти шейдеры\n");
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if(FragmentShaderStream.is_open()){
		std::stringstream sstr;
		sstr << FragmentShaderStream.rdbuf();
		FragmentShaderCode = sstr.str();
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;


	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}



	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}



	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("Error: %s\n", &ProgramErrorMessage[0]);
	}
	
	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);
	
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	printf("Done \n");
	return ProgramID;
}


class Brunke
{
public:
	glm::vec3 startpoint;
	glm::vec3 endpoint;
	glm::vec3 points[4];

	static unsigned short triangl_seq_brunke[12];
	static float h_scale;

	Brunke(glm::vec3 startpoint, glm::vec3 endpoint)
	{
		glm::vec3 zero(0.f, s2, 0.f);
		glm::vec3 one(-1.f, -1.f, 0.f);
		glm::vec3 two(1.f, -1.f, 0.f);
		float k = glm::length(endpoint - startpoint) / this->h_scale;

		this->points[0] = glm::vec3(startpoint + (zero * k));
		this->points[1] = glm::vec3(startpoint + (one * k));
		this->points[2] = glm::vec3(startpoint + (two * k));
		this->points[3] = glm::vec3(endpoint);

		this->startpoint = startpoint;
		this->endpoint = endpoint;
	}
	Brunke(Brunke *source)
	{
		this->startpoint = source->startpoint;
		this->endpoint = source->endpoint;

		this->points[0] = source->points[0];
		this->points[1] = source->points[1];
		this->points[2] = source->points[2];
		this->points[3] = source->points[3];
	}

	Brunke transform(glm::mat4 transformation)
	{
		this->startpoint = glm::vec3(transformation * glm::vec4(this->startpoint, 1));
		this->endpoint = glm::vec3(transformation * glm::vec4(this->endpoint, 1));

		this->points[0] = glm::vec3(transformation * glm::vec4(this->points[0], 1));
		this->points[1] = glm::vec3(transformation * glm::vec4(this->points[1], 1));
		this->points[2] = glm::vec3(transformation * glm::vec4(this->points[2], 1));
		this->points[3] = glm::vec3(transformation * glm::vec4(this->points[3], 1));
		return this;
	}

	std::vector<unsigned short> index_return(unsigned short *iter)
	{
		std::vector<unsigned short> res;

		for (unsigned short i = 0; i < 12; i++)
			res.emplace_back(this->triangl_seq_brunke[i] + *iter);

		*iter += 4;

		return res;
	}

	std::vector<float> points_return()
	{
		std::vector<float> res;

		res.emplace_back(this->points[0].x);
		res.emplace_back(this->points[0].y);
		res.emplace_back(this->points[0].z);
		res.emplace_back(this->points[1].x);
		res.emplace_back(this->points[1].y);
		res.emplace_back(this->points[1].z);
		res.emplace_back(this->points[2].x);
		res.emplace_back(this->points[2].y);
		res.emplace_back(this->points[2].z);
		res.emplace_back(this->points[3].x);
		res.emplace_back(this->points[3].y);
		res.emplace_back(this->points[3].z);

		return res;
	}
};
float Brunke::h_scale = 15.f;
unsigned short Brunke::triangl_seq_brunke[12] = {0, 1, 2, 0, 1, 3, 0, 2, 3, 1, 2, 3};

class Brick
{
public:
	glm::vec3 startpoint;
	glm::vec3 endpoint;
	glm::vec3 points[8];

	static unsigned short triangl_seq_brick[36];
	static float h_scale;

	Brick(glm::vec3 startpoint, glm::vec3 endpoint)
	{
		glm::vec3 zero(-1.f, 1.f, 0.f);
		glm::vec3 one(1.f, 1.f, 0.f);
		glm::vec3 two(1.f, -1.f, 0.f);
		glm::vec3 three(-1.f, -1.f, 0.f);

		float k = glm::length(endpoint - startpoint) / this->h_scale;

		this->points[0] = glm::vec3(startpoint + (zero * k));
		this->points[1] = glm::vec3(startpoint + (one * k));
		this->points[2] = glm::vec3(startpoint + (two * k));
		this->points[3] = glm::vec3(startpoint + (three * k));
		this->points[4] = glm::vec3(endpoint + (zero * k));
		this->points[5] = glm::vec3(endpoint + (one * k));
		this->points[6] = glm::vec3(endpoint + (two * k));
		this->points[7] = glm::vec3(endpoint + (three * k));

		this->startpoint = startpoint;
		this->endpoint = endpoint;
	}
	Brick(Brick *source)
	{
		this->startpoint = source->startpoint;
		this->endpoint = source->endpoint;

		for (short i = 0; i < 8; i++)
			this->points[i] = source->points[i];
	}

	Brick transform(glm::mat4 transformation)
	{
		this->startpoint = glm::vec3(transformation * glm::vec4(this->startpoint, 1));
		this->endpoint = glm::vec3(transformation * glm::vec4(this->endpoint, 1));

		for (unsigned short i = 0; i < 8; i++)
			this->points[i] = glm::vec3(transformation * glm::vec4(this->points[i], 1));

		return this;
	}

	std::vector<unsigned short> index_return(unsigned short *iter)
	{
		std::vector<unsigned short> res;

		for (unsigned short i = 0; i < 36; i++)
			res.emplace_back(this->triangl_seq_brick[i] + *iter);

		*iter += 8;

		return res;
	}

	std::vector<float> points_return()
	{
		std::vector<float> res;

		for (short i = 0; i < 8; i++)
		{
			res.emplace_back(this->points[i].x);
			res.emplace_back(this->points[i].y);
			res.emplace_back(this->points[i].z);
		}

		return res;
	}
	std::vector<float> normal_colors_return()
	{
		std::vector<float> res;

		glm::vec3 normal = glm::normalize(this->points[0] - this->points[6]);
		res.emplace_back(normal.x);
		res.emplace_back(normal.y);
		res.emplace_back(normal.z);

		normal = glm::normalize(this->points[1] - this->points[7]);
		res.emplace_back(normal.x);
		res.emplace_back(normal.y);
		res.emplace_back(normal.z);

		normal = glm::normalize(this->points[2] - this->points[4]);
		res.emplace_back(normal.x);
		res.emplace_back(normal.y);
		res.emplace_back(normal.z);

		normal = glm::normalize(this->points[3] - this->points[5]);
		res.emplace_back(normal.x);
		res.emplace_back(normal.y);
		res.emplace_back(normal.z);

		normal = glm::normalize(this->points[4] - this->points[2]);
		res.emplace_back(normal.x);
		res.emplace_back(normal.y);
		res.emplace_back(normal.z);

		normal = glm::normalize(this->points[5] - this->points[3]);
		res.emplace_back(normal.x);
		res.emplace_back(normal.y);
		res.emplace_back(normal.z);

		normal = glm::normalize(this->points[6] - this->points[0]);
		res.emplace_back(normal.x);
		res.emplace_back(normal.y);
		res.emplace_back(normal.z);

		normal = glm::normalize(this->points[7] - this->points[1]);
		res.emplace_back(normal.x);
		res.emplace_back(normal.y);
		res.emplace_back(normal.z);

		return res;
	}
};
float Brick::h_scale = 10.f;
unsigned short Brick::triangl_seq_brick[36] = {0, 1, 2, 0, 2, 3, 0, 4, 7, 0, 7, 3, 2, 6, 1, 1, 6, 5, 4, 5, 6, 4, 6, 7, 3, 7, 6, 3, 6, 2, 0, 1, 4, 1, 4, 5};

glm::mat4 makeTransform(glm::vec3 startpoint, glm::vec3 endpoint, float scale_factor, float angle_x, float angle_rot)
{
	glm::mat4 gozero = glm::translate(glm::mat4(), startpoint * (-1.f));

	glm::mat4 scaling = glm::scale(glm::mat4(), glm::vec3(scale_factor, scale_factor, scale_factor));

	glm::vec3 myRotationAxis(1, 0, 0); //врощаем по x _ _! Если криво, то значит ошибка тут
	glm::mat4 rot_x = glm::rotate(glm::radians(angle_x), myRotationAxis);

	myRotationAxis = glm::vec3(endpoint - startpoint);
	glm::mat4 rot_rot = glm::rotate(glm::radians(angle_rot), myRotationAxis);

	glm::mat4 gofus = glm::translate(glm::mat4(), endpoint);

	return gofus * rot_rot * rot_x * scaling * gozero;
}

bool keys[1024];

glm::vec3 cameraPos = glm::vec3(0.0f, -2.0f, 1.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 1.0f, 0.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 0.0f, 1.0f);

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

GLfloat lastX = 500, lastY = 500;

GLfloat phy = glm::radians(90.f);
GLfloat thetta = glm::radians(90.f);
GLfloat fov = 45.0f;


//Функция обратного вызова для обработки событий клавиатуры
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		//Если нажата клавиша ESCAPE, то закрываем окно
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;
}
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS || mouse_click){
		GLfloat xoffset = xpos - lastX;
		GLfloat yoffset = ypos - lastY; 
		GLfloat sensitivity = def_camSens;
		
		xoffset *= sensitivity * x_mouse_inv;
		yoffset *= sensitivity * y_mouse_inv;

		phy   += xoffset;
		thetta += yoffset;

		glm::vec3 front;
		front.x = glm::sin(thetta) * glm::cos(phy);
		front.y = glm::sin(thetta) * glm::sin(phy);
		front.z = glm::cos(thetta);

		cameraFront = glm::normalize(front);

		lastX = xpos;
		lastY = ypos;
	}
}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	fov += yoffset * def_zoomSens;
  if(fov <= 1.0f)
  	fov = 1.0f;
  if(fov >= 90.0f)
  	fov = 90.0f;
}
void do_movement()
{
	// Camera controls
	GLfloat currentFrame = glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;

	GLfloat cameraSpeed = def_camSpeed * deltaTime;

	if (keys[GLFW_KEY_W])
		cameraPos += cameraSpeed * cameraFront;
	if (keys[GLFW_KEY_S])
		cameraPos -= cameraSpeed * cameraFront;
	if (keys[GLFW_KEY_A])
		cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
	if (keys[GLFW_KEY_D])
		cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}

int main()
{
	//Инициализируем библиотеку GLFW и Устанавливаем параметры создания графического контекста
	if (!glfwInit())
	{
		std::cerr << "ERROR: could not start GLFW3\n";
		exit(1);
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//Создаем графический контекст (окно)
	GLFWwindow *window = glfwCreateWindow(2000, 2000, "My CatBoost", nullptr, nullptr);
	if (!window)
	{
		std::cerr << "ERROR: could not open window with GLFW3\n";
		glfwTerminate();
		exit(1);
	}

	//Делаем этот контекст текущим
	glfwMakeContextCurrent(window);

	// Hide the mouse and enable unlimited mouvement
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	//Устанавливаем функцию обратного вызова для обработки событий клавиатуры
	glfwSetKeyCallback(window, keyCallback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	glfwSetCursorPos(window,1000, 1000);
	lastX = 1000;
	lastY = 1000;

	//Инициализируем библиотеку GLEW
	glewExperimental = GL_TRUE;
	glewInit();
	glClearColor(0.4f, 0.4f, 0.4f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	//Treeeee creation
	int brunke_num = 20;
	float scale = 0.5f;
	unsigned short iter = 0;

	std::vector<float> points;
	std::vector<float> colors;
	std::vector<unsigned short> indices;
	if (BRUNKE)
	{
		std::queue<Brunke> myque;

		Brunke stvol(glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 0.f, 1.f));
		myque.push(stvol);
		for (int i = 0; i < brunke_num; i++)
		{
			Brunke temp(myque.front());

			Brunke child_1(temp);
			child_1.transform(makeTransform(child_1.startpoint, child_1.endpoint, scale, 40.f, 10.f));
			myque.emplace(child_1);
			Brunke child_2(temp);
			child_2.transform(makeTransform(child_2.startpoint, child_2.endpoint, scale, 40.f, 130.f));
			myque.emplace(child_2);
			Brunke child_3(temp);
			child_3.transform(makeTransform(child_3.startpoint, child_3.endpoint, scale, 40.f, 250.f));
			myque.emplace(child_3);

			auto temp_pnt = temp.points_return();

			points.insert(points.end(), temp_pnt.begin(), temp_pnt.end());

			auto temp_ind = temp.index_return(&iter);

			indices.insert(indices.end(), temp_ind.begin(), temp_ind.end());
			myque.pop();
		}
	}

	if (BRICK)
	{
		std::queue<Brick> myque;

		Brick stvol(glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 0.f, 1.f));
		myque.push(stvol);
		for (int i = 0; i < brunke_num; i++)
		{
			Brick temp(myque.front());

			Brick child_1(temp);
			child_1.transform(makeTransform(child_1.startpoint, child_1.endpoint, scale, 40.f, 10.f));
			myque.emplace(child_1);
			Brick child_2(temp);
			child_2.transform(makeTransform(child_2.startpoint, child_2.endpoint, scale, 40.f, 130.f));
			myque.emplace(child_2);
			Brick child_3(temp);
			child_3.transform(makeTransform(child_3.startpoint, child_3.endpoint, scale, 40.f, 250.f));
			myque.emplace(child_3);

			auto temp_pnt = temp.points_return();

			points.insert(points.end(), temp_pnt.begin(), temp_pnt.end());

			auto temp_col = temp.normal_colors_return();

			colors.insert(colors.end(), temp_col.begin(), temp_col.end());

			auto temp_ind = temp.index_return(&iter);

			indices.insert(indices.end(), temp_ind.begin(), temp_ind.end());
			myque.pop();
		}
	}

	GLuint program = LoadShaders("777MelkumovData1/vs.vert", "777MelkumovData1/gs.frag");

	GLuint MatrixID = glGetUniformLocation(program, "MVP");

	GLuint VertexArray;
	glGenVertexArrays(1, &VertexArray);
	glBindVertexArray(VertexArray);
	printf("Fuck\n");

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(float), &points[0], GL_STATIC_DRAW);

	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	GLuint colorbuffer;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(float), &colors[0], GL_STATIC_DRAW);

	//Цикл рендеринга (пока окно не закрыто)
	while (!glfwWindowShouldClose(window))
	{
		glm::mat4 Projection = glm::perspective(glm::radians(fov), GLfloat(1.0f / 1.0f), GLfloat(0.001f), GLfloat(1000.0f));
		glm::mat4 View = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
		glm::mat4 Model = glm::mat4();
		// float radius = 0.7f;
		// glm::mat4 View = glm::lookAt(
		// 	glm::vec3(sin(glfwGetTime()) * radius, cos(glfwGetTime()) * radius, 0.5f), // Camera is at (4,3,-3), in World Space
		// 	glm::vec3(0, 0, 0.5),
		// 	glm::vec3(0, 0, 1));

		glm::mat4 MVP = Projection * View * Model;

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(program);

		//Проверяем события ввода (здесь вызывается функция обратного вызова для обработки событий клавиатуры)
		glfwPollEvents();
		do_movement();

		// Получаем размеры экрана (окна)
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		// Устанавливаем порт вывода на весь экран (окно)
		glViewport(0, 0, width, height);

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,		  // attribute. No particular reason for 0, but must match the layout in the shader.
			3,		  // size
			GL_FLOAT, // type
			GL_FALSE, // normalized?
			0,		  // stride
			(void *)0 // array buffer offset
		);

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
		glVertexAttribPointer(
			1,		  // attribute. No particular reason for 0, but must match the layout in the shader.
			3,		  // size
			GL_FLOAT, // type
			GL_FALSE, // normalized?
			0,		  // stride
			(void *)0 // array buffer offset
		);

		glDrawElements(
			GL_TRIANGLES,			 // mode
			GLsizei(indices.size()), // count
			GL_UNSIGNED_SHORT,		 // type
			(void *)0				 // element array buffer offset
		);

		glfwSwapBuffers(window); //Переключаем передний и задний буферы
	}

	//Удаляем созданные объекты OpenGL
	glDeleteProgram(program);
	glDeleteBuffers(1, &vertexbuffer);
	glfwTerminate();

	return 0;
}