#version 330 core

// Interpolated values from the vertex shaders
in vec3 fragmentColor;

// Ouput data
out vec3 color;

void main(){

	// Output color = color specified in the vertex shader, 
	// interpolated between all 3 surrounding vertices
	//color = vec3(sin(fragmentColor.z*0.3f),cos(fragmentColor.z*0.7f),sin(fragmentColor.z*0.1f)+cos(fragmentColor.z*0.1f));
	color = fragmentColor * 0.5 + 0.5;
}